import locationHelperBuilder from 'redux-auth-wrapper/history4/locationHelper'
import {connectedRouterRedirect} from "redux-auth-wrapper/history4/redirect";

const locationHelper = locationHelperBuilder({})

export const userIsAuthenticated = connectedRouterRedirect({
    redirectPath: "/login",
    authenticatedSelector: state => state.user.data !== null,
    wrapperDisplayName: "UserIsAuthenticated"
})

export const userIsNotAuthenticated = connectedRouterRedirect({
    redirectPath: (state, ownProps) => locationHelper.getRedirectQueryParam(ownProps) || '/',
    allowRedirectBack: false,
    authenticatedSelector: state => state.user.data === null,
    wrapperDisplayName: "UserIsNotAuthenticated"
})