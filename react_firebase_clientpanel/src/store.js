import {createStore, combineReducers, compose} from "redux";
import firebase from "firebase";
import "firebase/firestore";
import {reactReduxFirebase, firebaseReducer} from "react-redux-firebase";
import {reduxFirestore, firestoreReducer} from "redux-firestore";
import notifyReducer from "./reducers/notifyReducer";
import settingsReducer from "./reducers/settingsReducer";

const firebaseConfig = {
    apiKey: "AIzaSyAPCXcARtBGiummisZVY23inmwefrTBKes",
    authDomain: "react-client-panel-7a924.firebaseapp.com",
    databaseURL: "https://react-client-panel-7a924.firebaseio.com",
    projectId: "react-client-panel-7a924",
    storageBucket: "react-client-panel-7a924.appspot.com",
    messagingSenderId: "573938180685",
    appId: "1:573938180685:web:9bda5a52c4ff46e87bb204"
};

const rrfConfig = {
    userProfile: "users",
    useFirestoreForProfile: true
};

firebase.initializeApp(firebaseConfig);

const fireStore = firebase.firestore();
const settings = {timestampsInSnapshots: true};

fireStore.settings(settings);

const createStoreWithFirebase = compose(
    reactReduxFirebase(firebase, rrfConfig),
    reduxFirestore(firebase)
)(createStore);
const rootReducer = combineReducers({
    firebase: firebaseReducer,
    fireStore: firestoreReducer,
    notify: notifyReducer,
    settings: settingsReducer
});

if (localStorage.getItem('settings') == null) {
    const defaultSettings = {
        disableBalanceOnAdd: true,
        disableBalanceOnEdit: false,
        allowRegistration: false
    }

    localStorage.setItem('settings', JSON.stringify(defaultSettings));
}

const initialState = {settings: JSON.parse(localStorage.getItem('settings'))};
const store = createStoreWithFirebase(
    rootReducer, initialState, compose(reactReduxFirebase(firebase),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
);

export default store;